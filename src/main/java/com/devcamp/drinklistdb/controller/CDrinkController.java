package com.devcamp.drinklistdb.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.drinklistdb.model.CDrink;
import com.devcamp.drinklistdb.repository.IDrinkRepository;

@RestController
@RequestMapping("/")
@CrossOrigin
public class CDrinkController {
    @Autowired
    IDrinkRepository iDrinkRepository;

    @GetMapping("/drinks")
    public ResponseEntity<List<CDrink>> getAllCDrinks(){
        try {
            List<CDrink> listDrink = new ArrayList<CDrink>();
            iDrinkRepository.findAll().forEach(listDrink::add);
            return new ResponseEntity<>(listDrink, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
