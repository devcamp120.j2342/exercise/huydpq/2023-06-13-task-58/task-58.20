package com.devcamp.drinklistdb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DrinkListDbApplication {

	public static void main(String[] args) {
		SpringApplication.run(DrinkListDbApplication.class, args);
	}

}
