package com.devcamp.drinklistdb.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.drinklistdb.model.CDrink;

public interface IDrinkRepository extends JpaRepository<CDrink, Long> {
    
}
